fine=100;

translate([0,23,20])
rotate ([0,90,0]) {
    
    // gerades Stück
    cube([20,12.5,2]);
    
    // Biegung
    translate([0,0,16])
    rotate ([90,180,90])
    intersection() {
        difference() {
          cylinder(h=20, r=16, $fn = fine);
          cylinder(h=20, r=14, $fn = fine);
        };
        cube ([16,16,20]);
    }

    // Box mit Aussparung
    translate ([0,-14,24])
        rotate([0,180,180])
        difference() {
            cube([20,9,8]);
            translate([2,8,0])
            rotate([90,0,0])
            linear_extrude(height = 8)
            polygon(points=[[1.75,0],[0,5.5],[16,5.5],[14.25,0]]);
        }


    // Lippe
    translate([10,47.5,0])
        rotate([0,0,180]) {
        intersection() {
           union()  {
            difference() {
                cylinder(h=8, r=37.3, $fn = fine);
                cylinder(h=8, r=35, $fn = fine);
            }

            translate([0,0,6])

                rotate_extrude(convexity = 1, $fn = fine)
                translate([37.3, 0, 0])
                polygon(points=[[0,0],[0.7,0],[0,2]]);
        }


        translate([-10,0,0])
        cube([20, 38, 8]);
        }
    }
}





